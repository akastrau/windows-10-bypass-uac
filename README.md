# README #

 Credits: https://enigma0x3.net/2016/07/22/bypassing-uac-on-windows-10-using-disk-cleanup/

    Implemented by: Adrian Kastrau

    Bypass UAC for Windows 10 - C# version

    Code updated for Windows 10 Anniversary Update

    Have fun!

You need external dll to inject.

# Usage: #

Win10_BypassUAC.exe DLL_TO_INJECT.dll and volia have fun!

![screen](https://bytebucket.org/akastrau/windows-10-bypass-uac/raw/72b25bf32e46ecb9b5358da2b23a24d4474024e3/Win10_BypassUAC/screen.jpg)